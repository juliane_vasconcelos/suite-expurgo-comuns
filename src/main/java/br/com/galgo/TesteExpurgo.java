package br.com.galgo;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import br.com.galgo.enumerator.Frente;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.DadosExpurgo;
import br.com.galgo.testes.recursos_comuns.teste.Teste;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;
import br.com.galgo.utils.LogUtils;
import br.com.galgo.utils.ValidacaoExpugo;

public class TesteExpurgo extends Teste {

	private final String PASTA_TESTE = "Expurgo";
	private final String PATH = "K:/Condominio_STI/001 - Sist Galgo/Organizacao/04 Plano de Trabalho/Infra/04 - Operação da Infraestrutura/Expurgo/Unificação/Testes/";
	private final String PRE_PATH = "pre_expurgo.csv";
	private final String POS_PATH = "pos_expurgo.csv";

	private List<DadosExpurgo> preExpurgoList;
	private List<DadosExpurgo> posExpurgoList;
	private Frente frente;

	@Before
	public void setUp() throws Exception {
		TesteUtils.configurar(Ambiente.PRODUCAO, PASTA_TESTE);
	}

	@Test
	@Category(TesteTransferencia.class)
	public void testeFrenteTransferencia() throws ErroAplicacao {
		this.setNomeTeste("testeFrenteTransferencia");
		frente = Frente.TRANSFERENCIA;

		preExpurgoList = ArquivoUtils.uploadListExpurgo(PATH + frente.getPath()
				+ PRE_PATH);
		posExpurgoList = ArquivoUtils.uploadListExpurgo(PATH + frente.getPath()
				+ POS_PATH);

		validarExpurgo();
	}

	@Test
	@Category(TesteTarifas.class)
	public void testeFrenteTarifas() throws ErroAplicacao {
		this.setNomeTeste("testeFrenteTarifas");
		frente = Frente.TARIFAS;
		preExpurgoList = ArquivoUtils.uploadListExpurgo(PATH + frente.getPath()
				+ PRE_PATH);
		posExpurgoList = ArquivoUtils.uploadListExpurgo(PATH + frente.getPath()
				+ POS_PATH);

		validarExpurgo();
	}

	@Test
	@Category(TesteAuditoria.class)
	public void testeFrenteAuditoria() throws ErroAplicacao {
		this.setNomeTeste("testeFrenteAuditoria");
		frente = Frente.AUDITORIA;
		preExpurgoList = ArquivoUtils.uploadListExpurgo(PATH + frente.getPath()
				+ PRE_PATH);
		posExpurgoList = ArquivoUtils.uploadListExpurgo(PATH + frente.getPath()
				+ POS_PATH);

		validarExpurgo();
	}

	@Test
	@Category(TesteComunicacao.class)
	public void testeFrenteComunicacao() throws ErroAplicacao {
		this.setNomeTeste("testeFrenteComunicacao");
		frente = Frente.COMUNICACAO;
		preExpurgoList = ArquivoUtils.uploadListExpurgo(PATH + frente.getPath()
				+ PRE_PATH);
		posExpurgoList = ArquivoUtils.uploadListExpurgo(PATH + frente.getPath()
				+ POS_PATH);

		validarExpurgo();
	}

	private void validarExpurgo() {
		ValidacaoExpugo validacaoExpugo = new ValidacaoExpugo();
		validacaoExpugo.validaTabelaPre(frente, preExpurgoList);
		validacaoExpugo.valida(frente, preExpurgoList, posExpurgoList);
	}

	@After
	public void tearDown() {
		LogUtils.logar(frente, preExpurgoList, posExpurgoList);
	}

}
