package br.com.galgo.utils;

import java.util.List;

import br.com.galgo.enumerator.Frente;
import br.com.galgo.testes.recursos_comuns.file.entidades.DadosExpurgo;

public class LogUtils {

	public static void logar(Frente frente, List<DadosExpurgo> preExpurgoList,
			List<DadosExpurgo> posExpurgoList) {
		for (DadosExpurgo tabelaPos : posExpurgoList) {
			for (DadosExpurgo tabelaPre : preExpurgoList) {
				if (tabelaPos.getNome().equalsIgnoreCase(tabelaPre.getNome())) {
					if (frente.contains(tabelaPos.getNome())) {
						System.out.println("Tabela: " + tabelaPos.getNome());
						System.out
								.println("Quantidade de Registros Pré-Expurgo: "
										+ tabelaPre.getQtdRegistro());
						System.out
								.println("Quantidade de Registros Pós-Expurgo: "
										+ tabelaPos.getQtdRegistro());
						System.out.println();
					}
				}
			}
		}
	}

}
