package br.com.galgo.utils;

import java.util.List;

import org.junit.Assert;

import br.com.galgo.enumerator.Frente;
import br.com.galgo.testes.recursos_comuns.file.entidades.DadosExpurgo;

public class ValidacaoExpugo {

	public void valida(Frente frente, List<DadosExpurgo> preExpurgoList,
			List<DadosExpurgo> posExpurgoList) {
		for (DadosExpurgo tabelaPos : posExpurgoList) {
			boolean expurgoOK;
			for (DadosExpurgo tabelaPre : preExpurgoList) {
				if (tabelaPos.getNome().equalsIgnoreCase(tabelaPre.getNome())) {
					if (frente.contains(tabelaPos.getNome())) {
						expurgoOK = validaFrenteExpurgada(tabelaPos);
						if (expurgoOK == false) {
							Assert.fail("[ERRO] Nao foram expurgados todos os registros da tabela "
									+ tabelaPos.getNome() + ".");
						}
					} else {
						expurgoOK = validaFrentesNaoExpurgadas(tabelaPre,
								tabelaPos);

						if (expurgoOK == false) {
							Assert.fail("[ERRO] Foram expurgados registros da tabela "
									+ tabelaPos.getNome()
									+ " que nao pertence a frente de "
									+ frente.name() + ".");
						}
					}
				}
			}
		}
	}

	public void validaTabelaPre(Frente frente,
			List<DadosExpurgo> preExpurgoList) {
		int qtdZeradaDeReg = 0;
		int qtdRegFrente = 0;
		for (DadosExpurgo tabelaPre : preExpurgoList) {
			if (frente.contains(tabelaPre.getNome())) {
				qtdRegFrente++;
				if (tabelaPre.getQtdRegistro() == 0) {
					qtdZeradaDeReg++;
				}
			}
		}

		if (qtdZeradaDeReg == qtdRegFrente) {
			Assert.fail("Não havia registros para serem expurgados na frente "
					+ frente);
		}
	}

	public boolean validaFrenteExpurgada(DadosExpurgo tabelaPos) {
		if (tabelaPos.getQtdRegistro() != 0) {
			return false;
		}
		return true;
	}

	public boolean validaFrentesNaoExpurgadas(DadosExpurgo tabelaPre,
			DadosExpurgo tabelaPos) {
		if (tabelaPre.getQtdRegistro() > tabelaPos.getQtdRegistro()) {
			return false;
		}
		return true;
	}

}
