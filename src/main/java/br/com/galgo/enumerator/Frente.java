package br.com.galgo.enumerator;

import java.util.Arrays;
import java.util.List;

public enum Frente {

	TRANSFERENCIA("Transferência/",//
			Arrays.asList(Tabela.ATIVOBO,//
					Tabela.COMPROMISSOEVENTOTRANSFERENCIABO,//
					Tabela.COMPROMISSOMONITOREVENTOBO,//
					Tabela.COMPROMISSOOCORRENCIATRANSFERENCIABO,//
					Tabela.EXTRATOBO,//
					Tabela.GRUPOATIVOSBO,//
					Tabela.GRUPOATIVOSDOCUMENTOBO,//
					Tabela.IDENTIFICADORINFORMACAOFINANCEIRABO,//
					Tabela.INDICADORINFORMACAONAOCONSUMIDABO,//
					Tabela.INFOANBIMABO,//
					Tabela.INFORMACAOFINANCEIRABO,//
					Tabela.LANCAMENTOEXTRATOBO,//
					Tabela.LANCAMENTOMOVIMENTACAOPERIODOBO,//
					Tabela.LANCAMENTOSALDOCERTIFICADOBO,//
					Tabela.MARCADORBO,//
					Tabela.MOVIMENTACAOPERIODOBO,//
					Tabela.PLCOTABO,//
					Tabela.POSICAOATIVOSBO,//
					Tabela.POSICAOATIVOSDOCUMENTOBO,//
					Tabela.SALDOCERTIFICADOBO,//
					Tabela.SALDOCONSOLIDADOBO,//
					Tabela.SALDOEXTRATOBO)//
	), //
	TARIFAS("Tarifas/",//
			Arrays.asList(Tabela.CONSOLIDADOBO,//
					Tabela.DEMONSTRATIVOBO,//
					Tabela.DEMONSTRATIVOTARIFABO,//
					Tabela.FATURABO,//
					Tabela.FATURADEMONSTRATIVOBO,//
					Tabela.REGISTROCOBRANCABO,//
					Tabela.REGISTROCOBRANCAPROCESSADOBO,//
					Tabela.TARIFAENTIDADEASSOCIACAOBO)//
	), //
	AUDITORIA("Auditoria/",//
			Arrays.asList(Tabela.HISTORICOBO,//
					Tabela.LOGBO,//
					Tabela.TRILHAAUDITORIABO)//
	),

	COMUNICACAO("Comunicação/",//
			Arrays.asList(Tabela.ALERTAPORTALBO,//
					Tabela.ALERTAPORTALSUBSTITUIDABO,//
					Tabela.CAMPOALTERADOBO,//
					Tabela.CONTEUDOMENSAGEMCOMUNICADOBO,//
					Tabela.FATOPORTALBO,//
					Tabela.MENSAGEMEMAILBO,//
					Tabela.MENSAGEMWEBSERVICEBO,//
					Tabela.REGISTROWSALERTABO,//
					Tabela.SOLICALERTAAUTOMSTATUSTRANSMISSAOBO,//
					Tabela.SOLICALERTAMANUALSTATUSTRANSMISSAOBO,//
					Tabela.SOLICFATORELEVAUTOMSTATUSTRANSMISSAOBO,//
					Tabela.SOLICFATORELEVMANUALSTATUSTRANSMISSAOBO,//
					Tabela.SOLICITACAOALERTAAUTOMATICOBO,//
					Tabela.SOLICITACAOALERTAMANUALBO,//
					Tabela.SOLICITACAOCOMUNICADOCOMPROMISSOBO,//
					Tabela.SOLICITACAOCOMUNICADOENTIDADEBO,//
					Tabela.SOLICITACAOCOMUNICADOENTIDADEPAPELBO,//
					Tabela.SOLICITACAOCOMUNICADOPAPELBO,//
					Tabela.SOLICITACAOCOMUNICADOREGISTROINVESTIMENTOBO,//
					Tabela.SOLICITACAOFATORELEVANTEAUTOMATICOBO,//
					Tabela.SOLICITACAOFATORELEVANTEMANUALBO)//
	);

	private List<Tabela> tabelaList;

	private String path;

	Frente(String path, List<Tabela> tabelaList) {
		this.path = path;
		this.tabelaList = tabelaList;
	}

	public List<Tabela> getTabelaList() {
		return tabelaList;
	}

	public String getPath() {
		return path;
	}

	public boolean contains(String nomeTabela) {
		for (Tabela tabela : tabelaList) {
			if (tabela.name().equalsIgnoreCase(nomeTabela)) {
				return true;
			}
		}
		return false;
	}

}
